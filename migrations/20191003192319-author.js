const Sequelize = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    const Authors = queryInterface.createTable('authors', {
        id: {
          type: Sequelize.UUID,
          primaryKey: true,
          allowNull: false,
          defaultValue: Sequelize.UUIDV4
        },
        image: {
          type: Sequelize.STRING,
          allowNull: false
        },
        full_name: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true
        }
      });
    return Authors;
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('authors');
  }
};

const Sequelize = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.createTable('users', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: Sequelize.UUIDV4
      },
      user_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      likes_list: {
        type: Sequelize.ARRAY(Sequelize.STRING),
        allowNull: false
      }
    })
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('users');
  }
};

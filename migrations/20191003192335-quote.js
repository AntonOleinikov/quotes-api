const Sequelize = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.createTable('quotes', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: Sequelize.UUIDV4
      },
      author_full_name: {
        allowNull: false,
        type: Sequelize.STRING,
        references: {
          model: 'authors',
          key: 'full_name'
        }
      },
      text: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      likes: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      }
    })
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('quotes');
  }
};

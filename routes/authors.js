const express = require('express');
const router = express.Router();
const db = require('../models');
const upload = require('../services/image-upload');

const singleUpload = upload.single('image');

router.get('/', async (req, res, next) => {
  const quotes = await db.Author.findAll({});
  res.send(quotes);
});

router.post('/', (req, res, next) => {
  singleUpload(req, res, async function(err) {
    if (err) {
      console.log('err--:>', err);
      return res.status(422).send({errors: [{title: 'Image Upload Error', detail: err.message}]});
    }

    try {
      const createdAuthor = await db.Author.create({
        image: req.file.location,
        full_name: req.body.full_name,
      });
      res.send({
        message: 'Author successfully added',
        data: createdAuthor
      });
    } catch (e) {
      next();
    }

  });
});

router.get('/quotes/:name/:offset', async (req, res, next) => {
  console.log('name', req.params.name);
  try {
    const quotes = await db.Quote.findAndCountAll(
      {
        offset: req.params.offset,
        limit: 10,
        include: [db.Author],
        where: {
          author_full_name: req.params.name
        }
      }
    );
    res.send(quotes);
  } catch (e) {
    next();
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    await db.Author.destroy(
      {
        where: {
          id: req.params.id
        }
      }
    );

    res.send({
      message: 'Quote successfully deleted',
      data: null
    });
  } catch (e) {
    next(e)
  }
});


module.exports = router;

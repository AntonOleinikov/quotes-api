const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const db = require('../models');


router.post('/detect', async (req, res, next) => {
  try {
    const {user_id} = req.body;

    const user = await db.User.findOrCreate(
      {
        where: {
          user_id
        },
        defaults: {
          user_id
        }
      }
    );

    res.send({user: user[0]});
  } catch (e) {
    console.error(e);
    next();
  }
});

router.post('/likes', async (req, res, next) => {
  console.log('CALL LIKES');
  try {
    const { user_id } = req.body;

    const likes = await db.Like.findAll(
      {
        where: {
          user_id
        }
      }
    );

    res.send(likes);
  } catch (e) {
    console.error(e);
    next();
  }
});


module.exports = router;

const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const {Op} = require('sequelize');
const db = require('../models');

router.get('/:offset', async (req, res, next) => {
  try {
    const quotes = await db.Quote.findAndCountAll(
      {
        offset: req.params.offset,
        limit: 10,
        include: [db.Author]
      }
    );
    res.send(quotes);
  } catch (e) {
    next();
  }
});

router.get('/search', async (req, res, next) => {
  try {
    const { q } = req.query;
    const searchResult = await db.Quote.findAll(
      {
        include: [db.Author],
        where: {
          [Op.or]: [
            {
              text: Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('text')), 'LIKE', '%' + q + '%')
            },
            {
              author_full_name: Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('author_full_name')), 'LIKE', '%' + q + '%')
            }
          ]
        }
      }
    );

    res.send({
      message: 'Quote successfully found',
      data: searchResult
    });
  } catch (e) {
    next(e)
  }
});

router.post('/', async (req, res, next) => {
  try {
    const {text, author_full_name} = req.body;
    const createdQuote = await db.Quote.create(
      {
        text,
        author_full_name
      }
    );

    res.send({
      message: 'Quote successfully created',
      data: createdQuote
    });
  } catch (e) {
    console.error(e);
    next();
  }
});

router.put('/:id', async (req, res, next) => {
  try {
    const {author_id, text, likes} = req.body;
    await db.Quote.update(
      {
        text,
        author_id,
        likes
      },
      {
        where: {id: req.params.id}
      }
    );
    res.send({
      message: 'Quote successfully updated',
      data: null
    });
  } catch (e) {
    next();
  }
});

router.patch('/:id/like', async (req, res, next) => {
  try {
    const { increment } = req.body;
    await db.Quote.update(
      {
        likes: Sequelize.literal(`likes ${increment ? '+' : '-'} 1`)
      },
      {
        where: {
          id: req.params.id
        }
      }
    );

    res.send({
      message: 'Quote successfully updated',
      data: null
    })
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    await db.Quote.destroy(
      {
        where: {
          id: req.params.id
        }
      }
    );

    res.send({
      message: 'Quote successfully deleted',
      data: null
    });
  } catch (e) {
    next(e)
  }
});

module.exports = router;

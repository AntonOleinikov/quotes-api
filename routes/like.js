const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const db = require('../models');

router.post('/', async (req, res, next) => {
  try {
    const { user_id, quote_id, increment } = req.body;

    console.log("/LIKE", {
      user_id,
      quote_id,
      increment
    });

    if (increment) {
      await db.Like.findOrCreate(
        {
          where: {
            user_id,
            quote_id
          },
          defaults: {
            user_id,
            quote_id
          }
        }
      );
    }
    else {
      await db.Like.destroy(
        {
          where: {
            quote_id,
            user_id
          }
        }
      );
    }

    res.send({ message: 'Quotes liked' });
  } catch (e) {
    console.error(e);
    next();
  }
});

module.exports = router;

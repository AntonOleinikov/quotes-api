'use strict';
const uuidv4 = require('uuid/v4');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('quotes', [{
      id: uuidv4(),
      text: 'Do the difficult things while they are easy and do the great things while they are small.',
      author_full_name: 'Lao Tzu',
      likes: 3
    }], {
      timestamps: false
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

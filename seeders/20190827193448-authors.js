'use strict';
const uuidv4 = require('uuid/v4');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('authors', [{
      id: '5f9286ce-e614-11e9-81b4-2a2ae2dbcce4',
      image: 'https://auxx.me/wp-content/uploads/2018/04/99-2.jpg',
      full_name: 'Lao Tzu',
    }], {
      timestamps: false
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

const Sequelize = require('sequelize');

module.exports = (sequelize) => {
  const User = sequelize.define('User', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: Sequelize.UUIDV4
      },
      user_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      likes_list: {
        type: Sequelize.ARRAY(Sequelize.STRING),
        defaultValue: []
      }
    },
    {
      modelName: 'user',
      timestamps: false,
      underscored: true
    });

  return User;
};

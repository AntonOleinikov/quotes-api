const Sequelize = require('sequelize');

module.exports = (sequelize) => {
  const Quote = sequelize.define('Quote', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: Sequelize.UUIDV4
      },
      text: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      likes: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      }
    },
    {
      modelName: 'quote',
      timestamps: false,
      underscored: true
    });

  Quote.associate = function (models) {
    Quote.belongsTo(models.Author, {foreignKey: 'author_full_name', targetKey: 'full_name'});
  };

  return Quote;
};

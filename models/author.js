const Sequelize = require('sequelize');

module.exports = (sequelize) => {
  const Author = sequelize.define('Author', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: Sequelize.UUIDV4
      },
      image: {
        type: Sequelize.STRING,
        allowNull: false
      },
      full_name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      }
    },
    {
      modelName: 'author',
      timestamps: false,
      underscored: true
    });

  Author.associate = function(models) {
    Author.hasMany(models.Quote, { foreignKey: 'author_full_name', sourceKey: 'full_name' });
  };

  return Author;
};

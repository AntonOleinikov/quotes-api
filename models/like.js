const Sequelize = require('sequelize');

module.exports = (sequelize) => {
  const Like = sequelize.define('Like', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: Sequelize.UUIDV4
      },
      user_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      quote_id: {
        type: Sequelize.STRING,
        allowNull: false
      }
    },
    {
      modelName: 'like',
      timestamps: false,
      underscored: true
    });

  return Like;
};
